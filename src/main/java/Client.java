import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Map;
import java.util.Scanner;

public class Client {
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    private Scanner scanner;

    public void connect(String ip, int port) {
        try {
            setupSocket(ip, port);
            sendRequestsToServer();
            stopConnection();
        } catch (IOException e) {
            System.out.println("Something went wrong.");
            stopConnection();
        }
    }

    private void setupSocket(String ip, int port) throws IOException {
        clientSocket = new Socket(ip, port);
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        System.out.println(in.readLine());
    }

    private void sendRequestsToServer() throws IOException {
        scanner = new Scanner(System.in);
        String text = null;
        do {
            System.out.println("\nEnter command (or \"help\" for information about available commands):");
            text = scanner.nextLine();
            out.println(text);
            JSONObject json = new JSONObject(in.readLine());
            printResponse(json);
        } while (!text.equals("stop"));
    }

    private void stopConnection() {
        System.out.println("Connection with server terminated.");
        try {
            in.close();
            out.close();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printResponse(JSONObject json) {
        Map<String, Object> responseAsMap = json.toMap();
        for (String key : responseAsMap.keySet()) {
            System.out.println(responseAsMap.get(key));
        }
    }

}
